var path = require("path");
var webpack = require("webpack");


module.exports = {
    entry: {
        app: "./src/main.ts",
        polyfill: "./src/polyfill.ts",
        vendor: "./src/vendor.ts"
    },
    output: {
        filename: "./app/static/js/[name].js"
    },
    watch: true,
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
          /angular(\\|\/)core(\\|\/)@angular/,
          path.resolve(__dirname, '../src')
        )
    ],
    resolve: {
        extensions: ['.js', '.ts']
    }
}
