aiofiles==0.3.1
appdirs==1.4.3
httptools==0.0.9
Jinja2==2.9.6
MarkupSafe==1.0
packaging==16.8
pkg-resources==0.0.0
pyparsing==2.2.0
sanic==0.5.2
six==1.10.0
ujson==1.35
uvloop==0.8.0
websockets==3.3
